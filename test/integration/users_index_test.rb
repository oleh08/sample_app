require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest

  def setup
    @admin     = users(:michael)
    @non_admin = users(:archer)
  end

  test 'index including pagination' do
    log_in_as(@admin)
    get users_path
    assert_template 'users/index'
    assert_select 'div.pagination', count: 2
    User.paginate(page: 1).each do |user|
      assert_select 'a[href=?]', user_path(user), text: user.name
      unless user == @admin
        assert_select 'a[href=?]', user_path(user), text: 'delete'
      end
    end
    assert_difference 'User.count', -1 do
      delete user_path(@non_admin)
    end
  end

  test 'index as non-admin' do
    log_in_as(@non_admin)
    get users_path
    assert_select 'a', text: 'delete', count: 0
  end

  test 'should not get users_path for non-activated and activated user' do
    post users_path, params: { user: { name: 'Example User',
                                       email: 'user@example.com',
                                       password:              'password',
                                       password_confirmation: 'password' } }
    user = assigns(:user)
    assert_not user.activated?
    get user_path(user)
    assert_redirected_to root_url
    get users_path
    assert_redirected_to login_path

    get edit_account_activation_path(user.activation_token, email: user.email)
    assert user.reload.activated?
    get users_path
    assert_template 'users/index'
    get user_path(user)
    assert_template 'users/show'
  end

end
