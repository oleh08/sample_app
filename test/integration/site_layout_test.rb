require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test 'layout links' do
    get root_path
    assert_template 'static_pages/home'
    assert_select 'a[href=?]', root_path, count: 2
    assert_select 'a[href=?]', help_path
    assert_select 'a[href=?]', about_path
    assert_select 'a[href=?]', contact_path
    get contact_path
    assert_select 'title', full_title('Contact')
    get signup_path
    assert_select 'title', full_title('Sign up')
    get help_path
    assert_template 'static_pages/help'
    assert_select 'title', full_title('Help')
    get login_path
    assert_select 'title', full_title('log in')
    assert_template 'sessions/new'
    log_in_as(@user)
    get user_path(@user)
    assert_select 'title', full_title(@user.name)
    assert_template 'users/show'
  end

end
